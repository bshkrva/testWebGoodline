package info.goodline.tests;

import info.goodline.pages.*;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


import java.util.concurrent.TimeUnit;


public class GoodlineNewsTest {

    public static WebDriver driver;
    public static MainPage mainPage;
    public static NewsPage newsPage;

    @BeforeClass
    public static void openBrowser() {
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        mainPage = new MainPage(driver);
        newsPage = new NewsPage(driver);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get("https://goodline.info/");
    }

    @Test
    public void openNews(){
        mainPage.clickOnNewsLink();
        newsPage.checkNumberOfNewsIsDisplayed(10);
        newsPage.loadMoreNews();
        newsPage.checkNumberOfNewsIsDisplayed(10);
        newsPage.checkNewsСontainsPicture();
    }

    @AfterClass
    public static void tearDown(){
        driver.quit();
    }
}
