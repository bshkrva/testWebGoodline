package info.goodline.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainPage {

    private WebDriver driver;

    public MainPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(partialLinkText = "Новости")
    private WebElement newsPageLink;

    public void clickOnNewsLink(){
        newsPageLink.click();
        System.out.println("click on news-link");
    }
}
