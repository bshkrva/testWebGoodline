package info.goodline.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class NewsPage {

    private WebDriver driver;
    public NewsPage(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBys({
            @FindBy(css = ".news"),
            @FindBy(css = ".block")
    })
    private List <WebElement> newsBlock;

    @FindBy(className = "more")
    private WebElement linkMoreNews;


    public void checkNumberOfNewsIsDisplayed(int countCompare){
        Integer countDisplayedNews  = 0;

        for(WebElement news: newsBlock) {
            if(news.isDisplayed()) {
                countDisplayedNews +=1;
            }
        }
        //Assert.assertTrue("countDisplayedNews" + countDisplayedNews, countDisplayedNews >=10);
        System.out.println("countDisplayedNews: " + countDisplayedNews);
        System.out.println("checkNumberOfNewsIsDisplayed: " + ((countDisplayedNews>=countCompare) ? "OK" : "NOT OK"));
    }

    public void loadMoreNews (){
        linkMoreNews.click();
        System.out.println("click more: OK");
    }

    public void checkNewsСontainsPicture(){
        Integer countPicture  = 0;

        for(WebElement news: newsBlock) {
            if(news.findElements(By.cssSelector(".news_img"))!=null) {
                countPicture +=1;
            }
        }
        System.out.println("countPicture: " + countPicture);
        System.out.println("checkNewsСontainsPicture: " + ((countPicture==newsBlock.size()) ? "OK" : "NOT OK"));
        //Assert.assertTrue("countPicture" + countPicture, countPicture==newsBlock.size());
    }
}
